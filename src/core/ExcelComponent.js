import { DOMListener } from '@core/DOMListener'

export class ExcelComponent extends DOMListener {
    constructor($root, options = {}) {
        super($root, options.listeners)
        this.name = options.name || ''
        this.store = options.store
        this.emitter = options.emitter
        this.subscribe = options.subscribe || []
        this.unsubscribers = []
        
        this.prepare()
    }

    // Calling before init
    prepare() {}

    // Returns component's template
    toHTML() {
        return ''
    }

    init() {
        this.initDOMListeners()
    }

    destroy() {
        this.removeDOMListeners()
        this.unsubscribers.forEach((unsubscribe) => unsubscribe())
    }

    // (Emitter) Subscribe to event
    $on(event, fn) {
        const unsubscribe = this.emitter.subscribe(event, fn)
        this.unsubscribers.push(unsubscribe)
    }

    // (Emitter) Notify listener about event
    $emit(event, ...args) {
        this.emitter.emit(event, ...args)
    }

    // (Redux) Call action
    $dispatch(action) {
        this.store.dispatch(action)
    }

    // Receiving only the data
    // which component subscribed to
    storeChanged() {}

    // Checking if component subscribed to
    // particular property of application state
    isWatching(key) {
        return this.subscribe.includes(key)
    }
}
