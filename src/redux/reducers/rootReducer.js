import {
    CHANGE_TITLE,
    TABLE_RESIZE,
    CHANGE_TEXT,
    APPLY_STYLE,
    CHANGE_STYLES
} from '../actions/types'

export function rootReducer(state, action) {
    let fieldType
    let val

    switch (action.type) {
        case CHANGE_TITLE:
            fieldType = 'excelTitle'
            return {
                ...state,
                [fieldType]: action.data
            }
        case TABLE_RESIZE:
            fieldType = action.data.type === 'col' ? 'colState' : 'rowState'
            return {
                ...state,
                [fieldType]: value(state, fieldType, action) 
            }
        case CHANGE_TEXT:
            fieldType = 'cellState'
            return {
                ...state,
                currentText: action.data.value,
                [fieldType]: value(state, fieldType, action) 
            }
        case APPLY_STYLE:
            fieldType = 'stylesState'
            val = state[fieldType] || {}
            action.data.ids.forEach((id) => {
                val[id] = { ...val[id], ...action.data.value }
            })

            return {
                ...state,
                [fieldType]: val,
                currentStyles: { ...state.currentStyles, ...action.data.value }
            }
        case CHANGE_STYLES:
            return {
                ...state,
                currentStyles: action.data
            }
        default:
            return state
    }
}

function value(state, fieldType, action) {
    const val = state[fieldType] || {}
    val[action.data.id] = action.data.value
    return val
}
