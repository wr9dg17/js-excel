import { storage } from '@core/utils'
import { defaultStyles } from '@/config/constants';

const defaultState = {
    excelTitle: '',
    colState: {},
    rowState: {},
    cellState: {},
    stylesState: {},
    currentText: '',
    currentStyles: defaultStyles
}

const normalize = (state) => ({
    ...state,
    currentStyles: defaultStyles,
    currentText: ''
})

export const initialState = storage('excel-state')
    ? normalize(storage('excel-state'))
    : defaultState 
