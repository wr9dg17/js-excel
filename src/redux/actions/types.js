export const CHANGE_TITLE = 'change_title'

export const APPLY_STYLE = 'apply_style'
export const CHANGE_STYLES = 'change_styles'

export const TABLE_RESIZE = 'table_resize'
export const CHANGE_TEXT = 'change_text'
