import {
    CHANGE_TITLE,
    TABLE_RESIZE,
    CHANGE_TEXT,
    APPLY_STYLE,
    CHANGE_STYLES
} from './types'

export function changeTitle(data) {
    return {
        type: CHANGE_TITLE,
        data
    }
}

export function tableResize(data) {
    return {
        type: TABLE_RESIZE,
        data
    }
}

export function changeText(data) {
    return {
        type: CHANGE_TEXT,
        data
    }
}

// data => { ids, value }
export function applyStyle(data) {
    return {
        type: APPLY_STYLE,
        data
    }
}

export function changeStyles(data) {
    return {
        type: CHANGE_STYLES,
        data
    }
}
