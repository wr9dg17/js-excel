import { createStore } from '@/redux/createStore'
import { rootReducer } from '@/redux/reducers/rootReducer'
import { initialState } from '@/redux/initialState'
import { Page } from '@core/Page'

import { Excel } from '@/containers/excel/Excel'
import { Header } from '@/components/header/Header'
import { Toolbar } from '@/components/toolbar/Toolbar'
import { Formula } from '@/components/formula/Formula'
import { Table } from '@/components/table/Table'

import { storage, debounce } from '@core/utils'

export class ExcelPage extends Page {
    getRoot() {
        const store = createStore(rootReducer, initialState)

        const stateListener = (state) => {
            storage('excel-state', state)
            console.log('Excel State: ', state)
        }

        store.subscribe(debounce(stateListener, 300))

        this.excel = new Excel({
            components: [Header, Toolbar, Formula, Table],
            store: store
        })

        return this.excel.getRoot()
    }

    afterRender() {
        this.excel.init()
    }

    destroy() {
        this.excel.destroy()
    }
}
