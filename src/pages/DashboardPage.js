import { $ } from '@core/dom'
import { Page } from '@core/page'
import { getAllRecords } from '@/components/dashboard/dashboard.functions'

export class DashboardPage extends Page {
    getRoot() {
        const now = Date.now().toString()

        return $.create('div', 'db').html(`
            <div class="db__header">
                <h1>Excel Dashboard</h1>
            </div>

            <div class="db__new">
                <div class="db__view">
                    <a href="#excel/${now}" class="db__create">
                        Новая <br /> таблица
                    </a>
                </div>
            </div>

            <div class="db__table db__view">
                <div class="db__list-header">
                    <span>Название</span>
                    <span>Дата oткрытия</span>
                </div>

                <ul class="db__list">
                    ${ getAllRecords() }
                </ul>
            </div>
        `)
    }
}
