export class TableSelection {
    static className = 'selected'

    constructor() {
        this.group = []
        this.current = null
    }

    // $el instanceof DOM
    select($el) {
        this.clearSelections()
        $el.focus().addClass(TableSelection.className)
        this.current = $el
        this.group.push($el)
    }

    get selectedIds() {
        return this.group.map(($el) => $el.getCellId())
    }

    selectGroup($group = []) {
        this.clearSelections()
        this.group = $group
        this.group.forEach(($el) => {
            $el.addClass(TableSelection.className)
        })
    }

    clearSelections() {
        this.group.forEach(($el) => {
            $el.removeClass(TableSelection.className)
        })
        this.group = []
    }

    applyStyles(styles) {
        this.group.forEach(($el) => {
            $el.css(styles)
        })
    }
}
