import { toInlineStyles } from '@core/utils'
import { parse } from '@core/parse'
import { defaultStyles } from '@/config/constants'

const CODES = {A: 65, Z: 90}
const CELL_WIDTH = 120
const CELL_HEIGHT = 26

function toChar(_, index) {
    return String.fromCharCode(CODES.A + index)
}

function getCellWitdh(state, index) {
    return (state[index] || CELL_WIDTH) + 'px'
}

function getRowHeight(state, index) {
    return (state[index] || CELL_HEIGHT) + 'px'
}

function createCell(state, row) {
    return function(_, index) {
        const id = `${row}:${index}`
        const value = state.cellState[id]
        const width = getCellWitdh(state.colState, index)
        const styles = toInlineStyles({
            ...defaultStyles,
            ...state.stylesState[id]
        })

        return `
            <div
                class="cell"
                contenteditable
                data-type="cell"
                data-col="${index}"
                data-id="${id}"
                data-value="${value || ''}"
                style="${styles}; width:${width}"
            >${parse(value) || ''}</div>
        `
    }
}

function createCol(state) {
    return function(col, index) {
        const width = getCellWitdh(state.colState, index)
        return `
            <div
                class="column"
                data-type="resizable"
                data-col="${index}"
                style="width:${width}"
            >
                ${col}
                <div class="col-resize" data-resize="col"></div>
            </div>
        `
    }
}

function createRow(content, index, state) {
    const height = getRowHeight(state.rowState, index)
    const resize = index
        ? '<div class="row-resize" data-resize="row"></div>'
        : ''

    return `
        <div
            class="row"
            data-type="resizable"
            data-row="${index}"
            style="height:${height}"
        >
            <div class="row-info">
                ${index ? index : ''}
                ${resize}
            </div>
            <div class="row-data">${content}</div>
        </div>
    `
}

export function createTable(rowsCount = 15, state = {}) {
    const colsCount = CODES.Z - CODES.A + 1
    const rows = []

    const cols = new Array(colsCount)
        .fill('')
        .map(toChar)
        .map(createCol(state))
        .join('')

    rows.push(createRow(cols, null, state))

    for (let row = 0; row < rowsCount; row ++) {
        const cells = new Array(colsCount)
            .fill('')
            .map(createCell(state, row))
            .join('')
            
        rows.push(createRow(cells, row + 1, state))
    }

    return rows.join('')
}
