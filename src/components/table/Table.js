import { ExcelComponent } from '@core/ExcelComponent'
import { $ } from '@core/dom'
import { TableSelection } from './TableSelection'
import { createTable } from './table.template'
import { shouldResize, isCell, matrix, nextSelector } from './table.functions'
import { resizeHandler } from './table.resize'
import * as actions from '@/redux/actions'
import { parse } from '@core/parse'
import { defaultStyles } from '@/config/constants'

export class Table extends ExcelComponent {
    static className = 'excel__table'

    constructor($root, options) {
        super($root, {
            name: 'Table',
            listeners: ['mousedown', 'keydown', 'input'],
            ...options
        })
    }

    prepare() {
        this.selection = new TableSelection()
    }

    toHTML() {
        // 20 => number of rows
        return createTable(20, this.store.getState())
    }

    init() {
        super.init()

        this.selectCell(this.$root.find('[data-id="0:0"]'))
        
        this.$on('formula:input', (value) => {
            this.selection.current
                .attr('data-value', value)
                .text(parse(value))
            this.updateCellTextInStore(value)
        })

        this.$on('formula:keydown', () => {
            this.selection.current.focus()
        })

        this.$on('toolbar:applyStyles', (value) => {
            this.selection.applyStyles(value)
            
            this.$dispatch(actions.applyStyle({
                ids: this.selection.selectedIds,
                value
            }))
        })
    }

    selectCell($cell) {
        this.selection.select($cell)
        this.$emit('table:navigation', $cell)

        const styles = $cell.getStyles(Object.keys(defaultStyles))
        this.$dispatch(actions.changeStyles(styles))
    }

    updateCellTextInStore(value) {
        this.$dispatch(actions.changeText({
            id: this.selection.current.getCellId(),
            value
        }))
    }

    async resizeTable(event) {
        try {
            const data = await resizeHandler(this.$root, event)
            this.$dispatch(actions.tableResize(data))
        } catch (error) {
            console.warn('Resize error: ', error.message)
        }
    }

    onMousedown(event) {
        if (shouldResize(event)) {
            this.resizeTable(event)
        } else if (isCell(event)) {
            const $target = $(event.target)

            if (event.shiftKey) {
                const $cells = matrix($target, this.selection.current)
                    .map((id) => this.$root.find(`[data-id="${id}"]`))
                this.selection.selectGroup($cells)
            } else {
                this.selectCell($target)
            }
        }
    }

    onInput(event) {
        this.updateCellTextInStore($(event.target).text())
    }

    onKeydown(event) {
        const { key, shiftKey } = event
        const keys = [
            'Enter',
            'Tab',
            'ArrowLeft',
            'ArrowRight',
            'ArrowUp',
            'ArrowDown'
        ]

        if (keys.includes(key) && !shiftKey) {
            event.preventDefault()
            const currentCellId = this.selection.current.getCellId(true)
            const $nextCell = this.$root.find(nextSelector(key, currentCellId))
            this.selectCell($nextCell)
        }
    }
}
