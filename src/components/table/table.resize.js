import { $ } from '@core/dom'

export function resizeHandler($root, event) {
    return new Promise((resolve) => {
        const $resizer = $(event.target)
        const $parent = $resizer.closest('[data-type="resizable"]')
        const parentCoords = $parent.getCoords()
        const $cells = $root.findAll(`[data-col="${$parent.data.col}"]`)
        const type = $resizer.data.resize
        let delta
        let value

        $resizer.css({ opacity: 1 })

        document.onmousemove = (e) => {
            if (type === 'col') {
                delta = e.pageX - parentCoords.right
                value = parentCoords.width + delta

                $resizer.css({
                    right: -delta + 'px',
                    bottom: '-9999px'
                })
            } else {
                delta = e.pageY - parentCoords.bottom
                value = parentCoords.height + delta

                $resizer.css({
                    bottom: -delta + 'px',
                    right: '-9999px'
                })
            }
        }

        document.onmouseup = () => {
            document.onmousemove = null
            document.onmouseup = null

            if (type === 'col') {
                $cells.forEach((el) => $(el).css({ width: value + 'px' }))
            } else {
                $parent.css({ height: value + 'px' })
            }

            resolve({
                id: $parent.data[type],
                type,
                value
            })

            $resizer.css({
                bottom: 0,
                right: 0,
                opacity: 0
            })
        }
    })
}
